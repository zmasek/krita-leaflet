from krita import QWidget
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import (QComboBox, QFileDialog, QHBoxLayout, QLabel,
                             QMainWindow, QPushButton, QVBoxLayout)

from .app import KritaLeaflet

ZOOM_LEVELS = 8


class KritaLeafletWidget(QMainWindow):
    def __init__(self, parent=None):
        super(KritaLeafletWidget, self).__init__()

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        self.statusbar = self.statusBar()
        self.zoom_level = QComboBox()
        self.directory_dialog = QPushButton('Select Directory', self)
        self.label = QLabel()
        self.leafletize = QPushButton('Leafletize', self)

        for level in range(ZOOM_LEVELS):
            self.zoom_level.addItem(str(level))

        vertLayout = QVBoxLayout()
        horiLayout = QHBoxLayout()
        horiLayout.addWidget(self.directory_dialog)
        horiLayout.addStretch()
        horiLayout.addWidget(self.label)

        vertLayout.addLayout(horiLayout)
        vertLayout.addWidget(self.zoom_level)
        vertLayout.addWidget(self.leafletize)
        vertLayout.addWidget(self.statusbar)

        self.centralWidget().setLayout(vertLayout)

        self.mainapp = KritaLeaflet(self.statusbar)

        self.zoom_level.currentTextChanged.connect(self.set_zoom_level)

        self.leafletize.clicked.connect(self.mainapp.leafletize)
        self.directory_dialog.clicked.connect(self.show_directory_dialog)

    @pyqtSlot(str)
    def set_zoom_level(self, zoom_level):
        self.mainapp.zoom_level = zoom_level

    def show_directory_dialog(self):
        directory = QFileDialog.getExistingDirectory(self, 'Select Directory')
        self.mainapp.output_dir = str(directory)
        self.label.setText(str(directory))
