import os

from krita import InfoObject, Krita

BOX = 256
DIMENSIONS = (256, 512, 1024, 2048, 4096, 8192, 16384)


class KritaLeaflet(object):
    def __init__(self, statusbar):
        """
        Initialize slices with PNG preset properties.
        """
        self.document = Krita.instance().activeDocument()
        self.statusbar = statusbar
        self.zoom_level = 4
        self.output_dir = None
        self.export_properties = InfoObject()
        self.export_properties.setProperty('alpha', True)
        self.export_properties.setProperty('compression', 1)
        self.export_properties.setProperty('forceSRGB', True)
        self.export_properties.setProperty('indexed', False)
        self.export_properties.setProperty('interlaced', False)
        self.export_properties.setProperty('saveSRGBProfile', False)
        self.export_properties.setProperty('transparencyFillcolor', [255, 255, 255])

    def leafletize(self):
        """
        Prepare slices and export to a chosen folder.
        """
        dimension = pow(2, int(self.zoom_level) + 8)
        self.prepare_image(dimension)
        for z in range(int(self.zoom_level), -1, -1):
            for x in range(int(dimension / BOX)):
                for y in range(int(dimension / BOX)):
                    self.create_tile(z, x, y)
            self.statusbar.showMessage('Zooming out...')
            dimension /= 2
            self.document.scaleImage(
                dimension, dimension, self.document.xRes(), self.document.yRes(), 'Bicubic')
            self.document.waitForDone()
        self.statusbar.showMessage('Done!')

    def prepare_image(self, dimension):
        """
        Prepare the image by scaling to the adequate resolution.
        """
        self.statusbar.showMessage('Preparing the image...')
        width = self.document.width()
        height = self.document.height()
        if width < height:
            new_width = dimension
            new_height = new_width * height / width
        else:
            new_height = dimension
            new_width = new_height * width / height
        self.document.scaleImage(
            new_width, new_height, self.document.xRes(), self.document.yRes(), 'Bicubic')
        self.document.waitForDone()
        self.document.crop(0, 0, dimension, dimension)
        self.document.waitForDone()
        self.statusbar.showMessage('Image prepared.')

    def create_tile(self, z, x, y):
        """
        Create a tile in the calculated coordinates and save it to a disk.
        """
        self.statusbar.showMessage('Creating a tile...')
        new_image = self.document.clone()
        offset_x = x * BOX
        offset_y = y * BOX
        new_image.crop(offset_x, offset_y, BOX, BOX)
        new_image.waitForDone()
        output_path = self.get_output_path(z, x, y)
        new_image.setBatchmode(True)
        new_image.exportImage(output_path, self.export_properties)
        new_image.close()

    def get_output_path(self, z, x, y):
        """
        Create the path for saving an image. If the path folders don't exist, create them as well.
        """
        output_path = os.path.join(self.output_dir, str(z), str(x), str(y) + '.png')
        output_directory = os.path.dirname(output_path)
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)
        return output_path
