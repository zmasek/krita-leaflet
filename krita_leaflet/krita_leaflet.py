from krita import Extension, Krita

from .widget import KritaLeafletWidget


class KritaLeafletExtension(Extension):
    def __init__(self, parent):
        super().__init__(parent)
        self.widget = None

    def setup(self):
        pass

    def createActions(self, window):
        action = window.createAction("Leafletize", "Krita - Leaflet", "tools/scripts")
        action.triggered.connect(self.invoke_widget)

    def invoke_widget(self):
        self.widget = KritaLeafletWidget(self)
        self.widget.show()

Krita.instance().addExtension(KritaLeafletExtension(Krita.instance()))
