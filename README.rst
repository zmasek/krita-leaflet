=========================
Leaflet plug-in for Krita
=========================

Download the repository and put leaflet.py file in your Krita plug-ins directory. E.g. in ~/.local/share/krita/pykrita and run (or restart) Krita. If you're not sure which, you can check the menu bar in Krita; go to Settings -> Manage Resources and click Open Resource Folder button in the following window.

This plug-in requires you to have an open image in Krita. You can access the plug-in on the menu bar under Tools -> Scripts -> Krita - Leaflet. You need to choose the zoom level as an integer between 0 and 7 and a folder where to output the resulting tiles. The outputting messages are shown in the Status Bar on the bottom of the opened plug-in window. Like in the case of the sister GIMP plug-in, parallelizing the processing is not that effective. It might take a while to create the tiles depending on your machine so be patient.

Zoom level of 8 is the largest one since JPEG does not support images larger than 65536x65536 px and such an image would be 16 GB in RAM, which increases while processing. It is expected that you have an adequate amount, but considering having so much resources is rare, keep it low. The algorithmic complexity is high and the processing requirements and time increase with larger images. Otherwise, it might be better to have a series of separate images to process them and to manage output image names separately.

Since Leaflet JavaScript library is meant for using the resulting tiles, you can check out the `documentation <http://leafletjs.com/reference.html#tilelayer>`_ it has for the full implementation. That said, to get you started right away, you should have the Leaflet library imported in your HTML file along with the required CSS. You should also have a div tag with an id "map". The div should have a specified width and height. Then, in a script tag you might have something like this:

.. code-block ::

    var map = L.map('map').setView([0.0, -0.0], 4);
    L.tileLayer('./{z}/{x}/{y}.jpeg', {
        maxZoom: 4
    }).addTo(map);

The tile folders are supposed to be in the same folder the HTML is in.

Feel free to contribute.
